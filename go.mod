module gitlab.com/Foricher/aleiothubmqtt

go 1.14

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/gdexlab/go-render v1.0.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.2 // indirect
	gitlab.com/Foricher/aleiothub v0.0.9
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
)
