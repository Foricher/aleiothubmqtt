package main

import (
	"fmt"
	"log"
	"runtime"
	"time"

	"github.com/gdexlab/go-render/render"
	"gitlab.com/Foricher/aleiothub"
	aleiothubmqtt "gitlab.com/Foricher/aleiothubmqtt"
)

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v BiB", m.Alloc)
	fmt.Printf("\tTotalAlloc = %v BiB", m.TotalAlloc)
	fmt.Printf("\tSys = %v biB", m.Sys)
	fmt.Printf("\tNumGC = %v\n", m.NumGC)
}

func testDevice(deviceService aleiothub.DeviceService) {
	params := map[string]interface{}{
		"filter.type": "iot.light.simple",
	}
	var respCount = <-deviceService.GetDevicesCount(params)
	if respCount.Err != nil {
		log.Printf("GetDevices Count err %v", render.AsCode(respCount))
	} else {
		log.Printf("GetDevices Count  %v", render.AsCode(respCount))
	}
	resp0 := <-deviceService.GetDevices(0, 2, "full", params)
	if resp0.Err != nil {
		log.Printf("GetDevices err %v", render.AsCode(resp0.Err))
	} else {
		log.Printf("GetDevices %v", render.AsCode(resp0.Data))
	}

	resp1 := <-deviceService.GetDevice("sga$100$shades$shade", nil)
	if resp1.Err != nil {
		log.Printf("GetDevice err %v", render.AsCode(resp1.Err))
	} else {
		log.Printf("GetDevice res=%v", render.AsCode(resp1.Data))
	}
	resp2 := <-deviceService.DeleteDevice("device_id_test", nil)
	if resp2.Err != nil {
		log.Printf("DeleteDevice err %v", render.AsCode(resp2.Err))
	} else {
		log.Printf("DeleteDevice res=%v", render.AsCode(resp2.Data))
	}

	data := map[string]interface{}{
		"id":   "device_id_test",
		"type": "iot.light.simple",
		"name": "test",
		"sections": map[string]interface{}{
			"config": map[string]interface{}{
				"p1": "1234",
			},
		},
	}

	resp3 := <-deviceService.CreateDevice(data, nil)
	if resp3.Err != nil {
		log.Printf("CreateDevice err %v", render.AsCode(resp3.Err))
	} else {
		log.Printf("CreateDevice res=%v", render.AsCode(resp3.Data))
	}

	resp4 := <-deviceService.GetDevice("device_id_test", nil)
	if resp4.Err != nil {
		log.Printf("GetDevice err %v", render.AsCode(resp4.Err))
	} else {
		log.Printf("GetDevice res=%v", render.AsCode(resp4.Data))
	}

	resp5 := <-deviceService.GetDevice("bad_device_id_test", nil)
	if resp5.Err != nil {
		log.Printf("GetDevice err %v", render.AsCode(resp5.Err))
	} else {
		log.Printf("GetDevice res=%v", render.AsCode(resp5.Data))
	}

	resp6 := <-deviceService.DeleteDevice("device_id_test", nil)
	if resp6.Err != nil {
		log.Printf("DeleteDevice err %v", render.AsCode(resp6.Err))
	} else {
		log.Printf("DeleteDevice res=%v", render.AsCode(resp6.Data))
	}

}

func testSubscriber(subscriberService aleiothub.SubscriberService) {
	subscription := &aleiothub.Subscriber{
		Name:          "go sample test",
		GatewayTopics: []string{"test"},
		DeviceTopics: []aleiothub.DeviceTopic{
			{
				ID: "all",
				Selectors: []map[string]interface{}{{
					"type": "iot.light.simple",
				}},
				Filters: []aleiothub.DeviceFilter{{Section: "state"}},
			},
		},
	}
	var subscriberID string
	resp0 := <-subscriberService.CreateSubscriber(subscription, "")
	if resp0.Err != nil {
		log.Printf("CreateSubscriber err %v", render.AsCode(resp0.Err))
	} else {
		subscriberID = resp0.Data.ID
		log.Printf("CreateSubscriber res=%v", render.AsCode(resp0.Data))
	}
	resp1 := <-subscriberService.GetSubscriber(subscriberID)
	if resp1.Err != nil {
		log.Printf("GetSubscriber err %v", render.AsCode(resp1.Err))
	} else {
		log.Printf("GetSubscriber res=%v", render.AsCode(aleiothub.ConvertToSubscriber(resp1.Data)))
	}

	subscription.Name = "go sample test update"
	resp2 := <-subscriberService.UpdateSubscriber(subscriberID, subscription)
	if resp2.Err != nil {
		log.Printf("UpdateSubscriber err %v", render.AsCode(resp2.Err))
	} else {
		subscriberID = resp0.Data.ID
		log.Printf("UpdateSubscriber res=%v", render.AsCode(resp2.Data))
	}

	resp3 := <-subscriberService.GetSubscribers(0, -1, "full", nil)
	if resp3.Err != nil {
		log.Printf("GetSubscribers err %v", render.AsCode(resp3.Err))
	} else {
		log.Printf("GetSubscribers res=%v", render.AsCode(aleiothub.ConvertToSubscribers(resp3.Data)))
	}

}

func main() {
	log.Printf("Start")
	chanEvent := make(chan map[string]interface{})
	chanConnect := make(chan bool)
	chanIotHubConnect := make(chan aleiothubmqtt.IOTHUBChanConnect)
	//	client := aleiothub.NewClientWS("ws://localhost:9000/api/ws", nil, chanConnect, chanEvent)

	//	client := aleiothubmqtt.NewClientIOTHUBMqtt([]string{"ssl://172.27.134.235:9883"}, &aleiothubmqtt.IOTHUBOptions{
	//	mqttOpts := mqtt.NewClientOptions()
	//				ClientID: "client1",
	//				Username: "user",
	//				Password: "passwd",
	//				TLSConfig: &tls.Config{
	//					InsecureSkipVerify: true,
	//				},

	client := aleiothubmqtt.NewClientIOTHUBMqtt([]string{"tcp://172.27.134.235:1883"}, &aleiothubmqtt.IOTHUBOptions{
		//		HTTPProxy: "http://192.168.254.49:8080",
		RemoteTopic: "iothub",
		IRPCOptions: aleiothubmqtt.IRPCOptions{
			LocalTopic: "local",
			Debug:      false,
			//			MqttOpts:   mqttOpts,
		},
	}, chanConnect, chanEvent, chanIotHubConnect)

	deviceService := client.GetDeviceService()
	subscriberService := client.GetSubscriberService()

	log.Printf("start connect")
	err := <-client.Connect(-1)
	log.Printf("connected %v", err)
	/*
		for !client.IsConnected() {
			err := <-client.Connect(0)
			log.Printf("connected %v", err)
			if err != nil {
				timer := time.NewTimer(10 * time.Second)
				<-timer.C
			}
		}
	*/
	ticker := time.NewTicker(20 * time.Second)

	process := func() {
		testSubscriber(subscriberService)
		testDevice(deviceService)
	}

	log.Printf("STARTED")
	for {
		select {
		case evt := <-chanIotHubConnect:
			log.Printf("EVT chanIotHubConnect %v", evt)
		case evt := <-chanEvent:
			event := aleiothub.ConvertToEvent(evt)
			log.Println("EVT event=", render.AsCode(event))
		case connected := <-chanConnect:
			log.Printf("EVT Connected=%v", connected)
			if connected {
				go process()
			}
		case <-ticker.C:
			runtime.GC()
			PrintMemUsage()
			log.Printf("#goroutines: %d\n", runtime.NumGoroutine())
			log.Printf("Connected=%v, iothub connected=%v", client.IsConnected(), client.IsIotHubConnected())
			if client.IsConnected() {
				//				resp := <-deviceService.GetDevice("sga$100$shades$shade", true)
				resp := <-deviceService.GetDevices(0, 2, "full", nil)
				if resp.Err != nil {
					log.Printf("Timer GetDevice err %v", render.AsCode(resp.Err))
				} else {
					devices := aleiothub.ConvertToDevices(resp.Data)
					log.Printf("Timer GetDevice res=%v", render.AsCode(resp.Data))
					log.Printf("Timer GetDevice res=%v", render.AsCode(devices))
				}
			} else {
				log.Printf("Not Connected")
			}

		}
	}
}
