package main

import (
	"log"
	"sync"
	"time"

	aleiothubmqtt "gitlab.com/Foricher/aleiothubmqtt"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	var wg sync.WaitGroup
	wg.Add(1)
	ticker := time.NewTicker(20 * time.Second)

	chanConnect := make(chan bool, 1)
	/*
		go func() {
			for {
				select {
				case connected := <-chanConnect:
					log.Println("Connected=", connected)
				}
			}
		}()
	*/
	client := aleiothubmqtt.NewClientMqtt([]string{"tcp://172.27.134.235:1883"}, &aleiothubmqtt.IRPCOptions{
		//		HTTPProxy: "http://192.168.254.49:8080",
		LocalTopic: "local",

		Debug: false,
	}, chanConnect)
	err := <-client.Connect(0)
	log.Printf("connected %v", err)
	/*
		data := struct {
			Number int    `json:"number"`
			Text   string `json:"text"`
		}{
			42,
			"Hello world!",
		}
	*/
	var cb1 = func(chanCb chan interface{}, params interface{}, header *aleiothubmqtt.RPCHeader) {
		log.Printf("cb1 %v, %v", params, header)
		chanCb <- map[string]interface{}{
			"p1": "v1",
			"p2": 3,
		}
	}

	client.RegisterMethod("devices/getDevices", cb1)

	//	res := <-client.CallMethod("local", "test", data)
	//	log.Println(res)
	/*
		data = struct {
			Number int    `json:"number"`
			Text   string `json:"text"`
		}{
			42,
			"Hello world!",
		}
	*/
	filter := map[string]interface{}{
		"filter.type": "iot.light.simple",
		"format":      "short",
		"limit":       5,
	}

	res := <-client.CallMethod("local", "devices/getDevices", filter, nil)

	log.Printf("result %v", res)

	for {
		select {
		case connected := <-chanConnect:
			log.Println("Connected=", connected)
			//		default:
			//			log.Println("no message received")

		case <-ticker.C:
			if client.IsConnected() {
				//				resp := <-deviceService.GetDevice("sga$100$shades$shade", true)
				resp := <-client.CallMethod("local", "devices/getDevices", filter, nil)
				if resp.Error != nil {
					log.Printf("Timer GetDevice err %v", resp.Error)
				} else {
					log.Printf("Timer GetDevice res=%v", resp.Result)
				}
			} else {
				log.Printf("Not Connected")
			}
		}
	}
	wg.Wait()
}
