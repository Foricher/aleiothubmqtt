package aleiothubmqtt

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	aleiothub "gitlab.com/Foricher/aleiothub"
)

type IOTHUBOptions struct {
	IRPCOptions
	RemoteTopic string
	PingTimeout time.Duration
}

type IOTHUBChanConnect struct {
	Connected bool
	Message   string
}

/*
type IIOTHUBClientMQTT struct {
    Url  string ;
    Connected  bool ;
    Events : EventEmitter ;
    LocalTopic : string | undefined;
	MqttRpc : IMqttRpc
}

export class DefaultIOTHUBOptions extends DefaultRPCOptions implements IIOTHUBOptions {
    remoteTopic = 'iothub';
    localTopic = 'local';
    requestTimeout = 60000;
    encoding = 'json';
    rpcClientMode = true;
    rpcServerMode = true;
    debug = false;
    refreshTimeout = 30;
}
*/

type IOTHUBMqttClient interface {
	GetURLs() []string
	GetLocalTopic() string
	GetChanEvent() chan map[string]interface{}
	SetChanEvent(chanEvent chan map[string]interface{})
	GetChanIotHubConnect() chan IOTHUBChanConnect
	SetChanIotHubConnect(chanIotHubConnect chan IOTHUBChanConnect)
	GetOptions() *IOTHUBOptions

	IsConnected() bool
	Connect(timeout time.Duration) chan error
	Disconnect()
	CallMethod(method string, params interface{}, remoteTopic *string) chan aleiothub.ResponseErr
	CallMethodTimeout(method string, params interface{}, timeout time.Duration, remoteTopic *string) chan aleiothub.ResponseErr
	RegisterMethod(methodName string, cb func(chanCb chan interface{}, params interface{}, header *RPCHeader)) error
	UnregisterMethod(methodName string) error
	UnregisterAllMethods()
	Log(text string)

	IsIotHubConnected() bool
	GetDeviceService() aleiothub.DeviceService
	GetDeviceSchemaService() aleiothub.SchemaDeviceService
	GetGatewayService() aleiothub.GatewayService
	GetSubscriberService() aleiothub.SubscriberService

	this() *iothubMqttClient
}

type SubscriberStore map[string]aleiothub.Subscriber

type iothubMqttClient struct {
	mqtt                 MQTTClient
	opts                 *IOTHUBOptions
	chanEvent            chan map[string]interface{}
	chanIotHubConnect    chan IOTHUBChanConnect
	mutexStore           sync.Mutex
	subscriberStore      SubscriberStore
	tickerPing           *time.Ticker
	chanStopPing         chan bool
	iotHubConnected      bool
	mutexIotHubConnected sync.Mutex
}

func NewClientIOTHUBMqtt(urls []string, options *IOTHUBOptions,
	chanConnect chan bool,
	chanEvent chan map[string]interface{},
	chanIotHubConnect chan IOTHUBChanConnect) IOTHUBMqttClient {
	opts := options
	if opts == nil {
		opts = &IOTHUBOptions{}
	}
	mqttClient := NewClientMqtt(urls, &opts.IRPCOptions, chanConnect)
	if opts.RemoteTopic == "" {
		opts.RemoteTopic = "iothub"
	}
	if opts.PingTimeout == 0 {
		opts.PingTimeout, _ = time.ParseDuration("30s")
	}
	client := &iothubMqttClient{mqtt: mqttClient, opts: opts, chanEvent: chanEvent, chanIotHubConnect: chanIotHubConnect, subscriberStore: make(SubscriberStore)}
	client.registerIotHubEvent()
	client.registerSubscriberMethod()
	return client
}

func (c *iothubMqttClient) this() *iothubMqttClient {
	return c
}

func (c *iothubMqttClient) GetURLs() []string {
	return c.mqtt.GetURLs()
}
func (c *iothubMqttClient) GetOptions() *IOTHUBOptions {
	return c.opts
}
func (c *iothubMqttClient) GetLocalTopic() string {
	return c.opts.LocalTopic
}

func (c *iothubMqttClient) GetChanEvent() chan map[string]interface{} {
	return c.chanEvent
}
func (c *iothubMqttClient) SetChanEvent(chanEvent chan map[string]interface{}) {
	c.chanEvent = chanEvent
}

func (c *iothubMqttClient) GetChanIotHubConnect() chan IOTHUBChanConnect {
	return c.chanIotHubConnect
}
func (c *iothubMqttClient) SetChanIotHubConnect(chanIotHubConnect chan IOTHUBChanConnect) {
	c.chanIotHubConnect = chanIotHubConnect
}

func (c *iothubMqttClient) GetDeviceService() aleiothub.DeviceService {
	return NewDeviceService(c)
}
func (c *iothubMqttClient) GetDeviceSchemaService() aleiothub.SchemaDeviceService {
	return NewSchemaService(c)
}
func (c *iothubMqttClient) GetGatewayService() aleiothub.GatewayService {
	return NewGatewayService(c)
}
func (c *iothubMqttClient) GetSubscriberService() aleiothub.SubscriberService {
	return NewSubscriberService(c)
}

func (c *iothubMqttClient) Connect(timeout time.Duration) chan error {
	connChan := make(chan error)
	errCnx := <-c.mqtt.Connect(timeout)
	go func() {
		if errCnx == nil {
			go c.sendPing()
			c.startPing()
		}
		connChan <- errCnx
	}()
	return connChan
}
func (c *iothubMqttClient) IsConnected() bool {
	return c.mqtt.IsConnected()
}
func (c *iothubMqttClient) Disconnect() {
	c.mutexIotHubConnected.Lock()
	c.iotHubConnected = false
	c.mutexIotHubConnected.Unlock()
	c.stopPing()
	c.mqtt.Disconnect()
}

func (c *iothubMqttClient) IsIotHubConnected() bool {
	return c.iotHubConnected
}

func (c *iothubMqttClient) CallMethod(method string, params interface{}, remoteTopic *string) chan aleiothub.ResponseErr {
	return c.CallMethodTimeout(method, params, 0, remoteTopic)
}
func (c *iothubMqttClient) CallMethodTimeout(method string, params interface{}, timeout time.Duration, remoteTopic *string) chan aleiothub.ResponseErr {
	_remoteTopic := c.opts.RemoteTopic
	if remoteTopic != nil {
		_remoteTopic = *remoteTopic
	}
	responseErr := make(chan aleiothub.ResponseErr)
	go func() {
		result := <-c.mqtt.CallMethodTimeout(_remoteTopic, method, params, timeout, &c.opts.PublishOpts)
		if result.Error != nil {
			err := errors.New("unknown")
			switch result.Error.Data.(type) {
			case error:
				err = result.Error.Data.(error)
			case string:
				err = errors.New(result.Error.Data.(string))
			case map[string]interface{}:
				val, ok := result.Error.Data.(map[string]interface{})["error"].(string)
				if ok {
					err = errors.New(val)
				}
			default:
			}
			responseErr <- aleiothub.ResponseErr{
				Err: &aleiothub.ResponseError{
					Status: "NOK",
					Error:  err,
				},
			}
		} else {
			responseErr <- aleiothub.ResponseErr{
				Data: result.Result,
				Err:  nil,
			}
		}
	}()
	return responseErr
}

func (c *iothubMqttClient) RegisterMethod(methodName string, cb func(chanCb chan interface{}, params interface{}, header *RPCHeader)) error {
	return c.mqtt.RegisterMethod(methodName, cb)
}
func (c *iothubMqttClient) UnregisterMethod(methodName string) error {
	return c.mqtt.UnregisterMethod(methodName)
}
func (c *iothubMqttClient) UnregisterAllMethods() {
	c.mqtt.UnregisterAllMethods()
}
func (c *iothubMqttClient) Log(text string) {
	c.mqtt.Log(text)
}

func (c *iothubMqttClient) registerIotHubEvent() {
	c.RegisterMethod("systems/event", func(chanCb chan interface{}, params interface{}, header *RPCHeader) {
		p, _ := params.(map[string]interface{})
		if p != nil {
			status, _ := p["status"].(map[string]interface{})
			if status != nil {
				connected, _ := status["connected"].(bool)
				message, _ := status["message"].(string)
				c.mutexIotHubConnected.Lock()
				defer c.mutexIotHubConnected.Unlock()
				if c.iotHubConnected != connected {
					c.iotHubConnected = connected
					c.emitEventConnected(c.iotHubConnected, message)
				}
			}
		}
	})
}

func (c *iothubMqttClient) registerSubscriberMethod() {
	c.RegisterMethod("subscribers/event", func(chanCb chan interface{}, params interface{}, header *RPCHeader) {
		var message map[string]interface{}
		messMap := params.(map[string]interface{})
		event, _ := messMap["event"].(string)
		err := json.Unmarshal([]byte(event), &message)
		if err != nil {
			c.Log(fmt.Sprintln("error:", err))
		}
		subscriberID, _ := message["subscriber_id"].(string)
		mess, _ := message["message"].(string)
		var subscriber *aleiothub.Subscriber
		if mess == "event" && subscriberID != "" {
			c.mutexStore.Lock()
			defer c.mutexStore.Unlock()
			subs, ok := c.subscriberStore[subscriberID]
			if ok {
				subscriber = &subs
			}
		}
		if subscriber != nil {
			c.chanEvent <- message
			c.Log("subscribers/event")
		}

		chanCb <- true
	})
}

func (c *iothubMqttClient) emitEventConnected(connected bool, message string) {
	if c.chanIotHubConnect != nil {
		go func() {
			c.chanIotHubConnect <- IOTHUBChanConnect{Connected: connected, Message: message}
		}()
	}
}

func (c *iothubMqttClient) sendPing() {
	//        this._mqttRpc.notifyMethod(this._remoteTopic, 'systems/ping', { subscribers: this._subscriberStore.getSubscribersAsArray() }, this._mqttRpc.RPCOpts.mqttPublishOpts);
	c.mutexStore.Lock()
	values := make([]map[string]interface{}, 0)
	for k, v := range c.subscriberStore {
		values = append(values, map[string]interface{}{
			"subscriberId": k,
			"subscription": v,
		})
	}
	c.mutexStore.Unlock()
	requestTimeout, _ := time.ParseDuration("5s")
	data := map[string]interface{}{
		"subscribers": values,
	}
	resp := <-c.CallMethodTimeout("systems/ping", data, requestTimeout, &c.GetOptions().RemoteTopic)
	if resp.Err != nil {
		if c.iotHubConnected {
			c.iotHubConnected = false
			c.emitEventConnected(c.iotHubConnected, resp.Err.Error.Error())
		}
	} else {
		data, _ := resp.Data.(map[string]interface{})
		status, _ := data["status"].(map[string]interface{})
		if status != nil {
			connected, _ := status["connected"].(bool)
			message, _ := status["message"].(string)
			c.mutexIotHubConnected.Lock()
			defer c.mutexIotHubConnected.Unlock()
			if c.iotHubConnected != connected {
				c.iotHubConnected = connected
				c.emitEventConnected(c.iotHubConnected, message)
			}
		}
	}
}

func (c *iothubMqttClient) startPing() {
	c.tickerPing = time.NewTicker(c.opts.PingTimeout)
	c.chanStopPing = make(chan bool)
	go func() {
		for {
			select {
			case <-c.chanStopPing:
				return
			case <-c.tickerPing.C:
				c.Log("sendPing")
				c.sendPing()
			}
		}
	}()
}

func (c *iothubMqttClient) stopPing() {
	if c.tickerPing != nil {
		c.tickerPing.Stop()
		c.tickerPing = nil
	}
	if c.chanStopPing != nil {
		c.chanStopPing <- true
		c.chanStopPing = nil
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type iothubMqttDeviceService struct {
	client IOTHUBMqttClient
}

func NewDeviceService(client IOTHUBMqttClient) aleiothub.DeviceService {
	deviceService := &iothubMqttDeviceService{client: client}
	return deviceService
}

func (c *iothubMqttDeviceService) GetDevices(offset int, limit int, format string, params map[string]interface{}) chan aleiothub.ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
		"params": params,
	}
	/*
		if params != nil {
			for k, v := range params {
				_params[k] = v
			}
		}
	*/
	return c.client.CallMethod("devices/getDevices", _params, &c.client.GetOptions().RemoteTopic)
	/*
		devicesErr := make(chan aleiothub.DevicesErr)
		go func() {
			res := <-c.client.CallMethod("devices/getDevices", p, &c.client.opts.RemoteTopic)
			if res.Err != nil {
				devicesErr <- aleiothub.DevicesErr{Err: res.Err}
			} else {
				var _devices = res.Data.([]interface{})
				var devices []*aleiothub.Device = make([]*aleiothub.Device, len(_devices))
				for i, d := range _devices {
					device := aleiothub.ConvertToDevice(d)
					devices[i] = device
				}
				devicesErr <- aleiothub.DevicesErr{Data: devices}
			}
		}()
		return devicesErr
	*/
}

func (c *iothubMqttDeviceService) GetDevicesCount(filters map[string]interface{}) chan aleiothub.CountErr {
	_params := map[string]interface{}{
		"params": filters,
	}
	countErr := make(chan aleiothub.CountErr)
	go func() {
		responseErr := <-c.client.CallMethod("devices/getDevicesCount", _params, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			countErr <- aleiothub.CountErr{Err: responseErr.Err}
		} else {
			countErr <- aleiothub.CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (c *iothubMqttDeviceService) CreateDevice(data interface{}, params map[string]interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"data":   data,
		"params": params,
	}
	//	return c.client.CallMethod("devices/createDevice", p, &c.client.opts.RemoteTopic)
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("devices/createDevice", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (c *iothubMqttDeviceService) UpdateDevice(deviceID string, data interface{}, params map[string]interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"deviceId": deviceID,
		"data":     data,
		"params":   params,
	}
	//	return c.client.CallMethod("devices/updateDevice", p, &c.client.opts.RemoteTopic)
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("devices/updateDevice", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (c *iothubMqttDeviceService) GetDevice(deviceID string, params map[string]interface{}) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"deviceId": deviceID,
		"params":   params,
	}
	return c.client.CallMethod("devices/getDevice", p, &c.client.GetOptions().RemoteTopic)
	/*
		deviceErr := make(chan aleiothub.DeviceErr)
		go func() {
			responseErr := <-c.client.CallMethod("devices/getDevice", p, &c.client.opts.RemoteTopic)
			if responseErr.Err != nil {
				deviceErr <- aleiothub.DeviceErr{Err: responseErr.Err}
			} else {
				deviceErr <- aleiothub.DeviceErr{Data: aleiothub.ConvertToDevice(responseErr.Data)}
			}
		}()
		return deviceErr
	*/
}

func (c *iothubMqttDeviceService) DeleteDevice(deviceID string, params map[string]interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"deviceId": deviceID,
		"params":   params,
	}
	//	return c.client.CallMethod("devices/deleteDevice", p, &c.client.opts.RemoteTopic)
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("devices/deleteDevice", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}
func (c *iothubMqttDeviceService) GetDeviceSection(deviceID string, section string, params map[string]interface{}) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"deviceId": deviceID,
		"section":  section,
		"params":   params,
	}
	return c.client.CallMethod("devices/getDeviceSection", p, &c.client.GetOptions().RemoteTopic)
}

func (c *iothubMqttDeviceService) UpdateDeviceSection(deviceID string, section string, data interface{}, params map[string]interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"deviceId": deviceID,
		"section":  section,
		"data":     data,
		"params":   params,
	}
	//	return c.client.CallMethod("devices/updateDeviceSection", p, &c.client.opts.RemoteTopic)
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("devices/updateDeviceSection", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type iothubMqttSchemaService struct {
	client IOTHUBMqttClient
}

func NewSchemaService(client IOTHUBMqttClient) aleiothub.SchemaDeviceService {
	schemaService := &iothubMqttSchemaService{client: client}
	return schemaService
}

func (c *iothubMqttSchemaService) GetSchemas(offset int, limit int, format string, params map[string]interface{}) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
		"params": params,
	}
	return c.client.CallMethod("schemas/getSchemas", p, &c.client.GetOptions().RemoteTopic)
	/*
		devicesErr := make(chan aleiothub.DevicesErr)
		go func() {
			res := <-c.client.CallMethod("devices/getDevices", p, &c.client.opts.RemoteTopic)
			if res.Err != nil {
				devicesErr <- aleiothub.DevicesErr{Err: res.Err}
			} else {
				var _devices = res.Data.([]interface{})
				var devices []*aleiothub.Device = make([]*aleiothub.Device, len(_devices))
				for i, d := range _devices {
					device := aleiothub.ConvertToDevice(d)
					devices[i] = device
				}
				devicesErr <- aleiothub.DevicesErr{Data: devices}
			}
		}()
		return devicesErr
	*/
}

func (c *iothubMqttSchemaService) GetSchemasCount() chan aleiothub.CountErr {
	countErr := make(chan aleiothub.CountErr)
	go func() {
		responseErr := <-c.client.CallMethod("schemas/getSchemasCount", nil, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			countErr <- aleiothub.CountErr{Err: responseErr.Err}
		} else {
			countErr <- aleiothub.CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (c *iothubMqttSchemaService) CreateSchema(data interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"data": data,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("schemas/createSchema", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (c *iothubMqttSchemaService) UpdateSchema(schemaID string, data interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"schemaId": schemaID,
		"data":     data,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("schemas/updateSchema", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (c *iothubMqttSchemaService) GetSchema(schemaID string) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"schemaId": schemaID,
	}
	return c.client.CallMethod("schemas/getSchema", p, &c.client.GetOptions().RemoteTopic)
}

func (c *iothubMqttSchemaService) DeleteSchema(schemaID string) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"schemaId": schemaID,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("schemas/deleteSchema", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type iothubMqttGatewayService struct {
	client IOTHUBMqttClient
}

func NewGatewayService(client IOTHUBMqttClient) aleiothub.GatewayService {
	gatewayService := &iothubMqttGatewayService{client: client}
	return gatewayService
}

func (c *iothubMqttGatewayService) GetGateways(offset int, limit int, format string, params map[string]interface{}) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
		"params": params,
	}
	return c.client.CallMethod("gateways/getGateways", p, &c.client.GetOptions().RemoteTopic)
}

func (c *iothubMqttGatewayService) GetGatewaysCount() chan aleiothub.CountErr {
	countErr := make(chan aleiothub.CountErr)
	go func() {
		responseErr := <-c.client.CallMethod("gateways/getSchemasCount", nil, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			countErr <- aleiothub.CountErr{Err: responseErr.Err}
		} else {
			countErr <- aleiothub.CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (c *iothubMqttGatewayService) CreateGateway(data interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"data": data,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("gateways/createGateway", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (c *iothubMqttGatewayService) UpdateGateway(gatewayID string, data interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"gatewayId": gatewayID,
		"data":      data,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("gateways/updateGateway", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (c *iothubMqttGatewayService) GetGateway(gatewayID string) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"gatewayId": gatewayID,
	}
	return c.client.CallMethod("gateways/getGateway", p, &c.client.GetOptions().RemoteTopic)
}

func (c *iothubMqttGatewayService) DeleteGateway(gatewayID string) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"gatewayId": gatewayID,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("gateways/deleteGateway", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

func (c *iothubMqttGatewayService) SendGatewayControllerStateToHub(gatewayID string, controllerURL string, stateData map[string]interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"gatewayId":     gatewayID,
		"controllerUrl": controllerURL,
		"stateData":     stateData,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("gateways/sendGatewayControllerStateToHub", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}
func (c *iothubMqttGatewayService) UpdatePartialGateway(gatewayID string, path string, data map[string]interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"gatewayId": gatewayID,
		"path":      path,
		"data":      data,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("gateways/updatePartialGateway", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type iothubMqttSubscriberService struct {
	client IOTHUBMqttClient
}

func NewSubscriberService(client IOTHUBMqttClient) aleiothub.SubscriberService {
	subscriberService := &iothubMqttSubscriberService{client: client}
	return subscriberService
}

func (c *iothubMqttSubscriberService) GetSubscribers(offset int, limit int, format string, params map[string]interface{}) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
		"params": params,
	}
	return c.client.CallMethod("subscribers/getSubscribers", p, &c.client.GetOptions().RemoteTopic)
}

func (c *iothubMqttSubscriberService) GetSubscribersCount() chan aleiothub.CountErr {
	countErr := make(chan aleiothub.CountErr)
	go func() {
		responseErr := <-c.client.CallMethod("subscribers/getSubscribersCount", nil, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			countErr <- aleiothub.CountErr{Err: responseErr.Err}
		} else {
			countErr <- aleiothub.CountErr{Count: uint(responseErr.Data.(map[string]interface{})["count"].(float64))}
		}
	}()
	return countErr
}

func (c *iothubMqttSubscriberService) CreateSubscriber(subscription interface{}, subscriberID string) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"subscription": subscription,
	}
	if subscriberID != "" {
		p["subscriberId"] = subscriberID
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("subscribers/createSubscriber", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			resp := aleiothub.ConvertToSuccessResponse(responseErr.Data)
			var subs aleiothub.Subscriber
			switch subscription.(type) {
			case map[string]interface{}:
				subs = *aleiothub.ConvertToSubscriber(subscription.(map[string]interface{}))
			case *aleiothub.Subscriber:
				subs = *subscription.(*aleiothub.Subscriber)
			case aleiothub.Subscriber:
				subs = subscription.(aleiothub.Subscriber)
			default:
				// no match; here v has the same type as i
			}
			subs.ID = resp.ID
			c.client.this().mutexStore.Lock()
			defer c.client.this().mutexStore.Unlock()
			c.client.this().subscriberStore[resp.ID] = subs
			successErr <- aleiothub.SuccessErr{Data: resp}
		}
	}()
	return successErr
}

func (c *iothubMqttSubscriberService) UpdateSubscriber(subscriberID string, subscription interface{}) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"subscriberId": subscriberID,
		"subscription": subscription,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("subscribers/updateSubscriber", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			resp := aleiothub.ConvertToSuccessResponse(responseErr.Data)
			var subs aleiothub.Subscriber
			switch subscription.(type) {
			case map[string]interface{}:
				subs = *aleiothub.ConvertToSubscriber(subscription.(map[string]interface{}))
			case *aleiothub.Subscriber:
				subs = *subscription.(*aleiothub.Subscriber)
			case aleiothub.Subscriber:
				subs = subscription.(aleiothub.Subscriber)
			default:
				// no match; here v has the same type as i
			}
			subs.ID = resp.ID
			c.client.this().mutexStore.Lock()
			defer c.client.this().mutexStore.Unlock()
			c.client.this().subscriberStore[resp.ID] = subs
			successErr <- aleiothub.SuccessErr{Data: resp}
		}
	}()
	return successErr
}

func (c *iothubMqttSubscriberService) GetSubscriber(subscriberID string) chan aleiothub.ResponseErr {
	p := map[string]interface{}{
		"subscriberId": subscriberID,
	}
	return c.client.CallMethod("subscribers/getSubscriber", p, &c.client.GetOptions().RemoteTopic)
}

func (c *iothubMqttSubscriberService) DeleteSubscriber(subscriberID string) chan aleiothub.SuccessErr {
	p := map[string]interface{}{
		"subscriberId": subscriberID,
	}
	successErr := make(chan aleiothub.SuccessErr)
	go func() {
		responseErr := <-c.client.CallMethod("subscribers/deleteSubscriber", p, &c.client.GetOptions().RemoteTopic)
		if responseErr.Err != nil {
			successErr <- aleiothub.SuccessErr{Err: responseErr.Err}
		} else {
			c.client.this().mutexStore.Lock()
			defer c.client.this().mutexStore.Unlock()

			delete(c.client.this().subscriberStore, subscriberID)
			successErr <- aleiothub.SuccessErr{Data: aleiothub.ConvertToSuccessResponse(responseErr.Data)}
		}
	}()
	return successErr
}
