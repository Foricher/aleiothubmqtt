package aleiothubmqtt

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

const (
	jsonrpcVersion      = "2.0"
	errorParse          = 32700  //	Parse error	Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.
	errorInvalid        = -32600 //	Invalid Request	The JSON sent is not a valid Request object.
	errorMethodNotFound = -32601 //	Method not found	The method does not exist / is not available.
	errorInvalidParams  = -32602 // Invalid params	Invalid method parameter(s).
	errorInternal       = -32603 // Internal error	Internal JSON-RPC error.
)

type RPCRequest struct {
	JSONRPC    string      `json:"jsonrpc"`
	ReplyTopic *string     `json:"$replyTopic"`
	SrcUUID    string      `json:"$srcUuid"`
	SrcTopic   string      `json:"$srcTopic"`
	Method     string      `json:"method"`
	Params     interface{} `json:"params,omitempty"`
	ID         *string     `json:"id"`
}

type RPCResponse struct {
	JSONRPC string      `json:"jsonrpc"`
	Result  interface{} `json:"result,omitempty"`
	Error   *RPCError   `json:"error,omitempty"`
	ID      string      `json:"id"`
}

type RPCError struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type RPCResultError struct {
	Result interface{} `json:"result,omitempty"`
	Error  *RPCError   `json:"error,omitempty"`
}

type RPCHeader struct {
	ReplyTopic *string `json:"$replyTopic"`
	SrcUUID    string  `json:"$srcUuid"`
	SrcTopic   string  `json:"$srcTopic"`
}

type PublishOptions struct {
	QOS      byte
	Retained bool
}
type SubscribeOptions struct {
	QOS byte
}

type IRPCOptions struct {
	LocalTopic          string
	ReplyTopic          string
	RequestTimeout      time.Duration
	MqttOpts            *mqtt.ClientOptions
	PublishOpts         PublishOptions
	SubscribeOpts       SubscribeOptions
	Debug               bool
	PayloadMaxSize      uint32
	PayloadCompressSize uint32
}

type ClientConfig struct {
	Instance string
	Prefix   string
	QoS      byte
}

type call struct {
	Req     *RPCRequest
	Timeout *time.Timer
	//	Res   response
	//	Res   map[string]interface{}
	Resp        chan RPCResultError
	timeoutDone chan bool
	//	Error error
}

type MQTTClient interface {
	//	SetChanEvent(chanEvent chan map[string]interface{})
	GetURLs() []string
	SetChanConnect(chanConnect chan bool)
	GetChanConnect() chan bool
	IsConnected() bool
	//	Connect() chan struct {
	//		bool
	//		error
	//	}
	Connect(timeout time.Duration) chan error
	Disconnect()

	NotifyMethod(remoteTopic string, method string, params interface{}, publishOpts *PublishOptions) error
	CallMethod(remoteTopic string, method string, params interface{}, publishOpts *PublishOptions) chan RPCResultError
	CallMethodTimeout(remoteTopic string, method string, params interface{}, timeout time.Duration, publishOpts *PublishOptions) chan RPCResultError
	RegisterMethod(methodName string, cb func(chanCb chan interface{}, params interface{}, header *RPCHeader)) error
	UnregisterMethod(methodName string) error
	UnregisterAllMethods()
	Log(text string)
	GetMQTT() mqtt.Client
}

type mqttClient struct {
	urls             []string
	Mqtt             mqtt.Client
	UUID             string
	srcTopic         string
	replyTopic       string
	remoteReplyTopic string
	Config           *ClientConfig
	Opts             *IRPCOptions
	pending          map[string]*call
	mutex            sync.Mutex
	methodRegistry   map[string]func(chanCb chan interface{}, params interface{}, header *RPCHeader)
	chanConnect      chan bool
	connected        bool
	//	connectTimer   *time.Ticker
}

func NewClientMqtt(urls []string, options *IRPCOptions, chanConnect chan bool) MQTTClient {
	opts := options
	if opts == nil {
		opts = &IRPCOptions{}
	}
	if opts.MqttOpts == nil {
		opts.MqttOpts = mqtt.NewClientOptions()
	}
	if opts.LocalTopic == "" {
		opts.LocalTopic = "local"
	}
	if opts.RequestTimeout == 0 {
		opts.RequestTimeout, _ = time.ParseDuration("30s")
	}
	if opts.MqttOpts.ClientID == "" {
		uuid, _ := uuid.NewUUID()
		opts.MqttOpts.ClientID = "MQTT-RPC_" + uuid.String()
	}
	if urls != nil {
		for _, url := range urls {
			opts.MqttOpts.AddBroker(url)
		}
	}
	srcTopic := opts.LocalTopic
	uuid, _ := uuid.NewUUID()
	replyTopic := strings.Join([]string{opts.LocalTopic, "reply", uuid.String()}, "/")
	remoteReplyTopic := replyTopic
	if opts.ReplyTopic != "" {
		remoteReplyTopic = strings.Join([]string{opts.ReplyTopic, "reply", uuid.String()}, "/")
		srcTopic = opts.ReplyTopic
	}
	//	connectTimer := time.NewTicker(5 * time.Second)
	client := &mqttClient{urls: urls, Mqtt: nil, Opts: options, UUID: uuid.String(),
		srcTopic:         srcTopic,
		replyTopic:       replyTopic,
		remoteReplyTopic: remoteReplyTopic,
		pending:          make(map[string]*call, 5),
		methodRegistry:   make(map[string]func(chanCb chan interface{}, params interface{}, header *RPCHeader), 50),
		chanConnect:      chanConnect,
		connected:        false,
		//		connectTimer:   connectTimer,
	}
	opts.MqttOpts.OnConnect = func(mqttClient mqtt.Client) {
		client.Log(fmt.Sprintf("OnConnect=%v", mqttClient.IsConnected()))
		client.updateConnected(true)
	}
	opts.MqttOpts.OnConnectionLost = func(mqttClient mqtt.Client, err error) {
		client.Log(fmt.Sprintf("OnConnectionLost=%v", mqttClient.IsConnected()))
		client.updateConnected(false)
	}
	opts.MqttOpts.AutoReconnect = true
	mqttCli := mqtt.NewClient(opts.MqttOpts)
	client.Mqtt = mqttCli

	/*
		go func() {
			for now := range connectTimer.C {
				client.checkConnected(now)
				fmt.Println(now, "checkConnected=", client.IsConnected())
			}
			fmt.Println("end checkConnected=", client.IsConnected())
		}()
	*/
	return client
}

func (c *mqttClient) GetURLs() []string {
	return c.urls
}

func (c *mqttClient) GetMQTT() mqtt.Client {
	return c.Mqtt
}

func (c *mqttClient) checkConnected(t time.Time) {
	if c.chanConnect != nil {
		con := c.IsConnected()
		if con != c.connected {
			c.chanConnect <- con
		}
	}
}

func (c *mqttClient) GetChanConnect() chan bool {
	return c.chanConnect
}
func (c *mqttClient) SetChanConnect(chanConnect chan bool) {
	c.chanConnect = chanConnect
}

func (client *mqttClient) IsConnected() bool {
	if client.Mqtt != nil {
		return client.Mqtt.IsConnectionOpen()
	}
	return false
}

func (client *mqttClient) updateConnected(connected bool) {
	client.mutex.Lock()
	defer client.mutex.Unlock()
	if client.connected != connected && client.chanConnect != nil {
		client.chanConnect <- connected
	}
	client.connected = connected
}

func (client *mqttClient) Connect(timeout time.Duration) chan error {
	client.Log("mqtt connect")
	connChan := make(chan error)
	go func() {
		currentTime := 0 * time.Second
		for !client.IsConnected() && (timeout < 0 || currentTime >= timeout) {
			if token := client.Mqtt.Connect(); token.Wait() && token.Error() != nil {
				client.updateConnected(client.Mqtt.IsConnected())
				currentTime += 10 * time.Second
				if timeout < 0 || currentTime < timeout {
					timer := time.NewTimer(10 * time.Second)
					<-timer.C
				} else {
					connChan <- token.Error()
				}
			} else {
				client.subscribeTopics()
				go client.updateConnected(client.Mqtt.IsConnected())
				connChan <- nil
			}
		}
	}()
	return connChan
}

func (client *mqttClient) subscribeTopics() {
	token := client.Mqtt.Subscribe(client.Opts.LocalTopic+"/request", client.Opts.SubscribeOpts.QOS, func(mqtt mqtt.Client, msg mqtt.Message) {
		//		if string(msg.Payload()) != "mymessage" {
		//				t.Fatalf("want mymessage, got %s", msg.Payload())
		//		}
		client.processRequest(mqtt, msg)
	})
	if token.Wait() && token.Error() != nil {
		client.Log(fmt.Sprintln("subscribeTopics request error ", token))
	}
	token = client.Mqtt.Subscribe(client.replyTopic, client.Opts.SubscribeOpts.QOS, func(mqtt mqtt.Client, msg mqtt.Message) {
		client.processReply(mqtt, msg)
	})
	if token.Wait() && token.Error() != nil {
		client.Log(fmt.Sprintln("subscribeTopics reply error ", token))
	}
}

func (client *mqttClient) publishResponse(replyTopic string, response *RPCResponse) {
	data, err := json.Marshal(response)
	if err != nil {
		client.Log(fmt.Sprintln("publishResponse error:", err))
	}
	token := client.Mqtt.Publish(replyTopic, client.Opts.PublishOpts.QOS, false, data)
	if token.Wait() && token.Error() != nil {
		client.Log(fmt.Sprintln("publishResponse error:", token.Error()))
	}
}

func (client *mqttClient) Log(text string) {
	if client.Opts.Debug {
		//		t := time.Now()
		//		log.Printf("MQTT-RPC [%s] %s", t.Format("2006-01-02 15:04:05.000"), text)
		log.Printf("MQTT-RPC %s", text)
	}
}

func toStruct(src interface{}, dst interface{}) {
	val := reflect.ValueOf(dst).Elem()
	for i := 0; i < val.NumField(); i++ {
		valueField := val.Field(i)
		typeField := val.Type().Field(i)
		tag := typeField.Tag

		fmt.Printf("Field Name: %s,\t Field Value: %v,\t Tag Value: %s\n", typeField.Name, valueField.Interface(), tag.Get("json"))
	}
}

func toRPCError(data map[string]interface{}) *RPCError {
	err := &RPCError{}
	code := data["code"]
	if code != nil {
		err.Code = int(code.(float64))
	}
	message := data["message"]
	if message != nil {
		err.Message = message.(string)
	}
	_data := data["data"]
	if _data != nil {
		err.Data = _data
	}
	return err
}

func toRPCResponse(data map[string]interface{}) *RPCResponse {
	resp := &RPCResponse{}
	ID := data["id"]
	if ID != nil {
		resp.ID = ID.(string)
	}
	jsonrpcVersion := data["jsonrpc"]
	if jsonrpcVersion != nil {
		resp.JSONRPC = jsonrpcVersion.(string)
	}
	result := data["result"]
	if result != nil {
		resp.Result = result
	}
	error := data["error"]
	if error != nil {
		resp.Error = toRPCError(error.(map[string]interface{}))
	}
	return resp
}

func toRPCRequest(data map[string]interface{}) *RPCRequest {
	req := &RPCRequest{}
	ID := data["id"]
	if ID != nil {
		_ID := ID.(string)
		req.ID = &_ID
	}
	jsonrpcVersion := data["jsonrpc"]
	if jsonrpcVersion != nil {
		req.JSONRPC = jsonrpcVersion.(string)
	}
	replyTopic := data["$replyTopic"]
	if replyTopic != nil {
		_replyTopic := replyTopic.(string)
		req.ReplyTopic = &_replyTopic
	}
	srcUUID := data["$srcUuid"]
	if srcUUID != nil {
		req.SrcUUID = srcUUID.(string)
	}
	srcTopic := data["$srcTopic"]
	if srcTopic != nil {
		req.SrcTopic = srcTopic.(string)
	}
	method := data["method"]
	if method != nil {
		req.Method = method.(string)
	}
	params := data["params"]
	if params != nil {
		req.Params = params
	}
	return req
}

func gUnzipData(data []byte) (resData []byte, err error) {
	b := bytes.NewBuffer(data)
	var r io.Reader
	r, err = gzip.NewReader(b)
	if err != nil {
		return
	}
	var resB bytes.Buffer
	_, err = resB.ReadFrom(r)
	if err != nil {
		return
	}
	resData = resB.Bytes()
	return
}

func gZipData(data []byte) (compressedData []byte, err error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	_, err = gz.Write(data)
	if err != nil {
		return
	}
	if err = gz.Flush(); err != nil {
		return
	}
	if err = gz.Close(); err != nil {
		return
	}
	compressedData = b.Bytes()
	return
}

func (client *mqttClient) compress(data []byte) []byte {
	if len(data) > int(client.Opts.PayloadCompressSize) {
		compressed, _ := gZipData(data)
		var encoded []byte
		base64.StdEncoding.Encode(encoded, compressed)
		compress := map[string]interface{}{
			"compress": "gzip/base64",
			"payload":  compressed,
		}
		compressData, err := json.Marshal(compress)
		if err != nil {
			client.Log(fmt.Sprintln("compress error:", err))
		}
		return compressData
	}
	return data
}

func (client *mqttClient) uncompress(data interface{}) interface{} {
	var result = data
	compress := data.(map[string]interface{})["compress"]
	payload := data.(map[string]interface{})["payload"]
	if payload != nil && compress == "gzip/base64" {
		var decoded []byte
		_, err := base64.StdEncoding.Decode(decoded, payload.([]byte))
		if err != nil {
			client.Log(fmt.Sprintln("uncompress error:", err))
			return nil
		}
		reader, _ := gUnzipData(decoded)
		err = json.Unmarshal(reader, &result)
		if err != nil {
			client.Log(fmt.Sprintln("error:", err))
		}
	}
	return result
}

func (client *mqttClient) processRequest(mqtt mqtt.Client, msg mqtt.Message) {
	var message interface{}
	err := json.Unmarshal(msg.Payload(), &message)
	if err != nil {
		fmt.Println("error:", err)
	}
	mess := client.uncompress(message)
	request := toRPCRequest(mess.(map[string]interface{}))
	client.Log(fmt.Sprintln("processRequest data:", request))
	var cb = client.methodRegistry[request.Method]
	if cb == nil {
		if request.ID != nil {
			response := &RPCResponse{ID: *request.ID, JSONRPC: jsonrpcVersion, Error: &RPCError{Code: errorMethodNotFound, Message: "method not found"}}
			client.publishResponse(*request.ReplyTopic, response)
		}
	} else {
		go func() {
			chanCb := make(chan interface{})
			go cb(chanCb, request.Params, &RPCHeader{ReplyTopic: request.ReplyTopic, SrcTopic: request.SrcTopic, SrcUUID: request.SrcUUID})
			res := <-chanCb
			//			log.Println(res)
			if request.ID != nil {
				response := &RPCResponse{ID: *request.ID, JSONRPC: jsonrpcVersion, Result: res}
				client.publishResponse(*request.ReplyTopic, response)
			}
		}()
	}
}

func (client *mqttClient) processReply(mqtt mqtt.Client, msg mqtt.Message) {
	var message interface{}
	err := json.Unmarshal(msg.Payload(), &message)
	if err != nil {
		client.Log(fmt.Sprintln("error:", err))
	}
	mess := client.uncompress(message)
	//	dst := RPCResponse{}
	//	toStruct(mess, &dst)
	reply := toRPCResponse(mess.(map[string]interface{}))
	client.mutex.Lock()
	defer client.mutex.Unlock()
	call := client.pending[reply.ID]
	if call == nil {
		client.Log(fmt.Sprintln("reply not found:", reply.ID))
		return
	}
	if call.Timeout != nil {
		call.timeoutDone <- true
		call.Timeout.Stop()
	}
	delete(client.pending, reply.ID)
	if reply.Error != nil {
		call.Resp <- RPCResultError{
			Result: nil,
			Error:  reply.Error,
		}
	} else {
		call.Resp <- RPCResultError{
			Result: reply.Result,
			Error:  nil,
		}
	}
}

/*
	ack := make(chan string, 1)
	connAck := strings.Join([]string{s.Config.Prefix, s.Config.Instance, "conn", "ack"}, "/")
	token := s.Mqtt.Subscribe(connAck, s.Config.QoS, func(mqttClient mqtt.Client, msg mqtt.Message) {
		ack <- string(msg.Payload())
	})
	if token.Wait() && token.Error() != nil {
		return token.Error()
	}
	conn := strings.Join([]string{s.Config.Prefix, instanceTo, "conn"}, "/")
	token = s.Mqtt.Publish(conn, s.Config.QoS, false, []byte(s.Config.Instance))
	if token.Wait() && token.Error() != nil {
		return token.Error()
	}

	select {
	case sessionId := <-ack:
		token := s.Mqtt.Unsubscribe(connAck)
		if token.Wait() && token.Error() != nil {
			return token.Error()
		}
		write := strings.Join([]string{s.Config.Prefix, instanceTo, s.Config.Instance, sessionId, "req"}, "/")
		read := strings.Join([]string{s.Config.Prefix, instanceTo, s.Config.Instance, sessionId, "res"}, "/")

		mqttConn := NewMqttRpcConn(s.Mqtt, read, write, s.Config.QoS)
		err := mqttConn.Open()
		if err != nil {
			return err
		}

		s.Client = jsonrpc.NewClient(mqttConn)

		return nil
	case <-time.After(time.Millisecond * time.Duration(timeout)):
		return errors.New("RPC client timeout")
	}
}
*/

func (client *mqttClient) CallMethod(remoteTopic string, method string, params interface{}, publishOpts *PublishOptions) chan RPCResultError {
	return client.CallMethodTimeout(remoteTopic, method, params, 0, publishOpts)
}

func (client *mqttClient) CallMethodTimeout(remoteTopic string, method string, params interface{}, timeout time.Duration, publishOpts *PublishOptions) chan RPCResultError {
	//	return client.Mqtt.Client.Call(method, params, reply)
	requestTimeout := timeout
	if requestTimeout == 0 {
		requestTimeout = client.Opts.RequestTimeout
	}
	requestID, _ := uuid.NewUUID()
	requestIDStr := requestID.String()
	request := &RPCRequest{JSONRPC: jsonrpcVersion, ReplyTopic: &client.remoteReplyTopic, SrcTopic: client.srcTopic,
		SrcUUID: client.UUID, ID: &requestIDStr, Method: method, Params: params}
	call := &call{
		Req:         request,
		Resp:        make(chan RPCResultError),
		timeoutDone: make(chan bool),
	}
	//	log.Println(call)
	go func() {
		data, err := json.Marshal(request)
		if err != nil {
			fmt.Println("error:", err)
			call.Resp <- RPCResultError{
				Result: nil,
				Error: &RPCError{
					Code:    errorParse,
					Data:    err,
					Message: "request publish error",
				},
			}
		}
		_publishOpts := publishOpts
		if _publishOpts == nil {
			_publishOpts = &client.Opts.PublishOpts
		}
		token := client.Mqtt.Publish(remoteTopic+"/request", _publishOpts.QOS, _publishOpts.Retained, data)
		if token.Wait() && token.Error() != nil {
			call.Resp <- RPCResultError{
				Result: nil,
				Error: &RPCError{
					Code:    errorInternal,
					Data:    token.Error().Error,
					Message: "request publish error",
				},
			}
		}
		client.mutex.Lock()
		client.pending[requestID.String()] = call
		call.Timeout = time.NewTimer(requestTimeout)
		client.mutex.Unlock()
		client.Log(fmt.Sprintf("CallMethod=%v", request))
		select {
		case <-call.timeoutDone:
		//		case <-time.After(30 * time.Second):
		case <-call.Timeout.C:
			//		call.Error = errors.New("request timeout")
			client.Log(fmt.Sprintf("CallMethod timeout request_id:%v\n", requestID.String()))
			client.mutex.Lock()
			delete(client.pending, requestID.String())
			client.mutex.Unlock()
			call.Resp <- RPCResultError{
				Result: nil,
				Error: &RPCError{
					Code: errorInternal,
					//					Code:   strconv.Itoa(errorInternal),
					//					Data:    errors.New("request timeout"),
					Data:    "request timeout",
					Message: "request_timeout",
				},
			}
		}
	}()
	return call.Resp
}

func (client *mqttClient) NotifyMethod(remoteTopic string, method string, params interface{}, publishOpts *PublishOptions) error {
	request := &RPCRequest{JSONRPC: jsonrpcVersion, SrcTopic: client.srcTopic,
		SrcUUID: client.UUID, Method: method, Params: params}
	data, err := json.Marshal(request)
	if err != nil {
		fmt.Println("error:", err)
		return err
	}
	_publishOpts := publishOpts
	if _publishOpts == nil {
		_publishOpts = &client.Opts.PublishOpts
	}
	token := client.Mqtt.Publish(remoteTopic+"/request", _publishOpts.QOS, _publishOpts.Retained, data)
	if token.Wait() && token.Error() != nil {
		return token.Error()
	}
	client.Log(fmt.Sprintf("NotifyMethod=%v", request))
	return nil
}

/*
func (client *MQTTClient) CallTimeout(method string, params interface{}, reply interface{}, timeout int) error {
	c := make(chan error, 1)
	go func() {
		//		c <- s.Client.Call(method, params, reply)
	}()
	select {
	case err := <-c:
		// use err and result
		return err
	case <-time.After(time.Millisecond * time.Duration(timeout)):
		// call timed out
		return errors.New("RPC timeout")
	}
}
*/

func (client *mqttClient) RegisterMethod(methodName string, cb func(chanCb chan interface{}, params interface{}, header *RPCHeader)) error {
	if client.methodRegistry[methodName] != nil {
		return fmt.Errorf("method %v already registered", methodName)
	} else {
		client.methodRegistry[methodName] = cb
		return nil
	}
}

func (client *mqttClient) UnregisterMethod(methodName string) error {
	if client.methodRegistry[methodName] == nil {
		return fmt.Errorf("method %v not registered", methodName)
	}
	delete(client.methodRegistry, methodName)
	return nil
}

func (client *mqttClient) UnregisterAllMethods() {
	client.methodRegistry = make(map[string]func(chanCb chan interface{}, params interface{}, header *RPCHeader), 50)
}

/// Close
func (client *mqttClient) Disconnect() {
	client.mutex.Lock()
	defer client.mutex.Unlock()
	client.methodRegistry = make(map[string]func(chanCb chan interface{}, params interface{}, header *RPCHeader), 50)
	for _, v := range client.pending {
		if v.Timeout != nil {
			v.timeoutDone <- true
			v.Timeout.Stop()
		}
	}
	client.pending = make(map[string]*call, 5)
	/*
		if client.connectTimer != nil {
			client.connectTimer.Stop()
		}
	*/
	client.Mqtt.Disconnect(0)
}
